<?php

namespace App\Controller;

use App\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{
  /**
   * @Route("/create-project")
   */
  public function createProject(): Response
  {
    $project = new Project();
    $project->setTitle('Titre du projet');
    $project->setDescription('Description du projet');

    $entityManager = $this->getDoctrine()->getManager();
    $entityManager->persist($project);
    $entityManager->flush();

    return new Response(sprintf('Projet %s crée', $project->getTitle()));
  }
}